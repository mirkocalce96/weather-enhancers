import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect } from 'react';
import MyLocationScreen from './source/screens/MyLocationScreen';
import SearchScreen from './source/screens/SearchScreen';
import HomeStack from './source/stacks/HomeStack';
import Octicons from 'react-native-vector-icons/Octicons';
import { COLOR_MAIN, API_APPID, DEFAULT_CITIES, API_URL_WEATHER, API_URL_ONE_CALL } from './source/utils/Constants';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import forecastReducer from './source/reducers/ForecastReducer';
import axios from 'axios';
import { createAddForecastAction, createAddOneCallAction } from './source/actions/ForecastActions';
import { SafeAreaProvider } from 'react-native-safe-area-context';

Octicons.loadFont();

const Tab = createBottomTabNavigator();
const store = configureStore({ reducer: forecastReducer });

const App = () => {

  useEffect(() => {
    setup();

    return () => {

    };
  }, []);

  const setup = () => {
    store.getState().cities.forEach(async element => {
      const url = API_URL_WEATHER + '?appid=' + API_APPID + '&units=metric' + '&q=' + element.name;

      const response = await axios.get(url)
      const forecast = response?.data;

      store.dispatch(createAddForecastAction(element.name, forecast));

      const urlOneCall = API_URL_ONE_CALL + '?appid=' + API_APPID + '&units=metric' + '&lat=' + forecast?.coord?.lat + '&lon=' + forecast?.coord?.lon

      axios.get(urlOneCall)
        .then((response) => {
          const oneCall = response?.data;

          store.dispatch(createAddOneCallAction(element.name, oneCall));
        })
    });
  };

  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <NavigationContainer>
          <Tab.Navigator
            safeAreaInsets={{ top: 0, left: 0, bottom: 0, right: 0 }}
            screenOptions={{
              headerShown: false,
              tabBarActiveTintColor: 'black',
              tabBarInactiveTintColor: 'gray',
              tabBarShowLabel: false,
              tabBarStyle: { position: 'absolute', height: 80, marginHorizontal: 20, marginBottom: 30, borderRadius: 25, shadowColor: 'black', shadowRadius: 20, shadowOpacity: 0.3, shadowOffset: { height: 0, width: 1 } },
            }}
          >
            <Tab.Screen
              name="HomeStack"
              component={HomeStack}
              options={{
                tabBarIcon: () => <Octicons name="home" size={30} color={COLOR_MAIN} />,
              }}
            />
            <Tab.Screen
              name="Search"
              component={SearchScreen}
              options={{
                tabBarIcon: () => <Octicons name="search" size={30} color={COLOR_MAIN} />,
              }}
            />
            <Tab.Screen
              name="MyLocation"
              component={MyLocationScreen}
              options={{
                tabBarIcon: () => <Octicons name="location" size={30} color={COLOR_MAIN} />,
              }}
            />
          </Tab.Navigator>
        </NavigationContainer>
      </Provider>
    </SafeAreaProvider>
  );
};

export default App;
