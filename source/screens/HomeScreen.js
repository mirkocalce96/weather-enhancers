import React, { useEffect } from 'react';
import { FlatList, StyleSheet, Text, View, Image, ActivityIndicator } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { API_URL_ICON, COLOR_MAIN, DATE_DAY, DATE_MONTHS } from '../utils/Constants';
import { connect } from 'react-redux';
import { TouchableOpacity } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import { createAddForecastAction } from '../actions/ForecastActions';
import { getGradientColors } from '../utils/Gradient';

FontAwesome.loadFont();

const HomeScreen = (props) => {

    const navigation = props.navigation;
    const nowDate = new Date()

    useEffect(() => {
        console.log('Home screen mounted');

        return () => {
        };
    }, []);

    const renderItem = ({ item, index }) => {
        const formattedDate = DATE_DAY[nowDate.getDay()] + ' ' + nowDate.getDate() + ', ' + DATE_MONTHS[nowDate.getMonth()];
        const formattedTime = nowDate.toLocaleTimeString('it-IT', { hour: 'numeric', minute: 'numeric', hour12: true, timeZone: item.info?.timezone })
        const iconUri = API_URL_ICON + item.current?.weather[0].icon + '@4x.png'
        const gradient = getGradientColors(item.current?.cod)

        function goDetail() {
            navigation?.navigate('DetailScreen', { title: item.current?.name, forecast: item });
        }

        return (
            <View style={styles.cityView}>
                {item.current && item.info ?
                    <TouchableOpacity onPress={goDetail} style={styles.cityTouchable}>
                        <LinearGradient style={styles.cityGradient} colors={gradient} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}>
                            <View style={styles.horizontalView}>
                                <Text numberOfLines={1} style={styles.renderNameText}>{item.current?.name}</Text>
                                <Text numberOfLines={2} style={[styles.renderNameText, { fontSize: 13 }]}>{formattedDate}</Text>
                                <Text numberOfLines={1} style={[styles.renderNameText, { fontSize: 11, fontWeight: 'normal', marginTop: 15 }]}>{formattedTime}</Text>
                            </View>
                            <View style={[styles.horizontalView, { justifyContent: 'center', alignItems: 'center' }]}>
                                <Image style={styles.iconImage} source={{ uri: iconUri }}></Image>
                            </View>
                            <View style={[{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }]}>
                                <Text style={[styles.renderNameText, { fontSize: 40 }]}>{Math.round(item.current?.main?.temp) + '°'}</Text>
                            </View>
                        </LinearGradient>
                    </TouchableOpacity>
                    :
                    <View style={styles.activityView}>
                        <ActivityIndicator size={'large'} color={COLOR_MAIN} style={{ alignSelf: 'center' }} />
                    </View>
                }
            </View>
        );
    };

    const ItemSeparatorComponent = () => <View style={{ height: 30 }} />;

    const addCity = () => {
    }

    return (
        <View style={styles.container}>
            <Text style={styles.welcomeText}>{'Good morning!\nMirko'}</Text>
            <TouchableOpacity style={styles.newTouchable} onPress={addCity}>
                <FontAwesome name='plus-square-o' size={35} color={COLOR_MAIN} />
                <Text style={[styles.welcomeText, { fontSize: 20 }]}>Aggiungi città</Text>
            </TouchableOpacity>
            <FlatList
                style={styles.cityFlatlist}
                contentContainerStyle={styles.cityFlatlistContentContainer}
                data={props.cities}
                renderItem={renderItem}
                ListHeaderComponent={ItemSeparatorComponent}
                ListFooterComponent={ItemSeparatorComponent}
                ItemSeparatorComponent={ItemSeparatorComponent}
                showsVerticalScrollIndicator={false}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 50,
        alignItems: 'center',
    },
    newTouchable: {
        marginTop: 20,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    welcomeText: {
        margin: 10,
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        color: COLOR_MAIN,
    },
    cityFlatlist: {
        height: '100%',
        width: '100%',
    },
    cityFlatlistContentContainer: {
        flexGrow: 1,
        paddingBottom: 70,
    },
    cityView: {
        height: 140,
        width: '100%',
        paddingHorizontal: 20,
        shadowColor: 'black',
        shadowRadius: 20,
        shadowOpacity: 0.3,
        shadowOffset: { height: 0, width: 1 },
    },
    cityTouchable: {
        height: '100%',
        borderRadius: 25,
        overflow: 'hidden',
    },
    cityGradient: {
        height: '100%',
        width: '100%',
        padding: 25,
        flexDirection: 'row'
    },
    renderNameText: {
        fontSize: 23,
        color: 'white',
        fontWeight: 'bold',
    },
    horizontalView: {
        flex: 1,
    },
    iconImage: {
        width: '95%',
        aspectRatio: 1
    },
    activityView: {
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const mapStateToProps = (state) => {
    return state;
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        // dispatching plain actions
        add: () => dispatch(createAddForecastAction()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
