import React from 'react'
import styled from 'styled-components/native'

const StyledView = styled.View`
    justify-content: center;
    align-items: center;
    flex: 1;
`

const StyledText = styled.Text`
  color: black;
  font-size: 20px;
  font-weight: bold;
`

const SearchScreen = () => {
    return (
        <StyledView>
            <StyledText>SEARCH</StyledText>
        </StyledView>
    )
}

export default SearchScreen