import React, { useEffect } from 'react'
import { StyleSheet, ScrollView, Text, StatusBar, View, Image, FlatList } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { getGradientColors } from '../utils/Gradient'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useHeaderHeight } from '@react-navigation/elements';
import { API_URL_ICON, DATE_DAY, DATE_MONTHS } from '../utils/Constants';

const DetailScreen = (props) => {

    const insets = useSafeAreaInsets()
    const forecast = props.route?.params?.forecast
    const headerHeight = useHeaderHeight()
    const gradient = getGradientColors(forecast.current?.cod)
    const nowDate = new Date()
    const formattedDate = DATE_DAY[nowDate.getDay()] + ' ' + nowDate.getDate() + ', ' + DATE_MONTHS[nowDate.getMonth()];
    const iconUri = API_URL_ICON + forecast.info?.current?.weather[0].icon + '@4x.png'

    useEffect(() => {
        console.log('Detail screen mounted');

        return () => {
        };
    }, []);

    const hourlyRenderItem = ({ item, index }) => {
        const date = new Date(item.dt * 1000)
        const isFirst = index == 0
        const formattedTime = date.toLocaleTimeString('it-IT', { hour: 'numeric', hour12: true, timeZone: item.info?.timezone })

        return (
            <View style={{ height: 120, overflow: 'visible' }} key={'hourly_' + index} >
                <View style={styles.hourlyVerticalWrapper}>
                    <Text style={[styles.hourlyHourText, { fontWeight: isFirst ? 'bold' : 'normal' }]}>{isFirst ? 'Now' : formattedTime}</Text>
                </View>
                <View style={styles.hourlyVerticalWrapper}>
                    <View style={index == 0 ? styles.hourlyFirstCardView : styles.hourlyCardView} />
                </View>
                <View style={styles.hourlyVerticalWrapper}>
                    <Text style={[styles.hourlyTemperatureText, { fontWeight: isFirst ? 'bold' : 'normal' }]}>{Math.round(item?.feels_like) + '°'}</Text>
                </View>
            </View>
        )
    }

    const renderHourlyForecast = () => {
        const renderItems = []
        const limits = 24

        forecast?.info?.hourly.forEach((item, index) => {
            if (index < limits) {
                renderItems.push(hourlyRenderItem({ item, index }))

                if (limits - 1 > index)
                    renderItems.push(hourlyItemSeparatorComponent(item, index))
            }
        })

        return renderItems
    }

    const hourlyItemSeparatorComponent = (item, index) => <View style={{ width: 50 }} key={'item_' + index} />;

    const dailyRenderItem = ({ item }) => {
        const day = DATE_DAY[new Date(item.dt * 1000).getDay()]

        return (
            <View style={styles.dailyCardView}>
                <Text style={styles.dailyCardDayText}>{day}</Text>
                <Text style={styles.dailyCardTemperatureText}>{Math.round(item?.feels_like.day) + '°'}</Text>
                <Image style={styles.dailyCardImage} source={{ uri: iconUri }} />
            </View>
        )
    }

    const dailyItemSeparatorComponent = () => <View style={{ width: 6 }} />;

    return (
        <LinearGradient style={{ flex: 1 }} colors={gradient} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}>
            <StatusBar barStyle='light-content' />
            <ScrollView style={styles.container} contentContainerStyle={[styles.contentContainer, { paddingTop: + headerHeight, paddingBottom: insets.bottom + 100 }]}>
                <Text style={styles.dateText}>{formattedDate}</Text>
                <Text style={styles.weatherText}>{forecast?.current?.weather[0]?.main}</Text>
                <View style={styles.temperatureView}>
                    <Image style={styles.image} source={{ uri: iconUri }} />
                    <Text style={styles.tempText}>{Math.round(forecast.info?.current?.temp) + '°'}</Text>
                </View>
                <ScrollView style={styles.hourlyScrollView} contentContainerStyle={styles.hourlyScrollViewContentContainer} horizontal={true} showsHorizontalScrollIndicator={false}>
                    <LinearGradient colors={['white', 'gray']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} style={styles.gradientView} >
                        {renderHourlyForecast()}
                    </LinearGradient>
                </ScrollView>
                <FlatList horizontal={true} data={forecast?.info?.daily} renderItem={dailyRenderItem} ListHeaderComponent={dailyItemSeparatorComponent} ListFooterComponent={dailyItemSeparatorComponent} showsHorizontalScrollIndicator={false} />
            </ScrollView>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        alignItems: 'center',
    },
    dateText: {
        marginTop: 20,
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold'
    },
    weatherText: {
        marginTop: 30,
        color: 'white',
        fontSize: 18
    },
    temperatureView: {
        marginTop: 30,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    image: {
        width: '35%',
        aspectRatio: 1
    },
    tempText: {
        color: 'white',
        fontSize: 100,
        fontWeight: 'bold'
    },
    gradientView: {
        height: 5,
        flexDirection: 'row',
        overflow: 'visible',
        justifyContent: 'center',
        alignItems: 'center'
    },
    hourlyScrollView: {
        height: 150,
        width: '100%',
        marginVertical: 30,
    },
    hourlyScrollViewContentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 50
    },
    hourlyVerticalWrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'visible'
    },
    hourlyHourText: {
        width: 50,
        height: 30,
        fontSize: 15,
        fontWeight: 'bold',
        color: 'white',
        marginBottom: 5,
        overflow: 'visible',
        position: 'absolute',
        textAlign: 'center'
    },
    hourlyTemperatureText: {
        width: 50,
        height: 30,
        fontSize: 24,
        fontWeight: 'bold',
        color: 'white',
        marginTop: 5,
        position: 'absolute',
        overflow: 'visible',
        textAlign: 'center'
    },
    hourlyFirstCardView: {
        height: 34,
        aspectRatio: 1,
        borderRadius: 17,
        backgroundColor: 'white'
    },
    hourlyCardView: {
        height: 22,
        aspectRatio: 1,
        borderRadius: 11,
        backgroundColor: 'white'
    },
    dailyCardView: {
        height: 250,
        aspectRatio: 3 / 5,
        borderRadius: 15,
        margin: 12,
        padding: 20,
        shadowColor: 'black',
        shadowRadius: 20,
        shadowOpacity: 1,
        shadowOffset: { height: 0, width: 0 },
        overflow: 'visible',
        backgroundColor: '#ffffff0f',
        alignItems: 'center'
    },
    dailyCardDayText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white'
    },
    dailyCardTemperatureText: {
        marginTop: 15,
        fontSize: 35,
        fontWeight: 'bold',
        color: 'white'
    },
    dailyCardImage: {
        marginTop: 15,
        width: '100%',
        aspectRatio: 1
    }
})

export default DetailScreen