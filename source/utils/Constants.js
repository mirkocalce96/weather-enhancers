const COLOR_MAIN = 'rgb(2,26,91)';

const DEFAULT_CITIES = [{ name: 'perth' }, { name: 'london' }, { name: 'naples' }];

const API_URL_WEATHER = 'http://api.openweathermap.org/data/2.5/weather';
const API_URL_ONE_CALL = 'https://api.openweathermap.org/data/2.5/onecall';
const API_URL_ICON = 'http://openweathermap.org/img/wn/'
const API_APPID = '48fa368eb7195b2dd925e36a7db77eec';

const DATE_DAY = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
const DATE_MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

export {
    COLOR_MAIN,
    DEFAULT_CITIES,
    API_URL_WEATHER,
    API_URL_ONE_CALL,
    API_URL_ICON,
    API_APPID,
    DATE_DAY,
    DATE_MONTHS,
};
