const getGradientColors = (id) => {
    if (id >= 700)
        return ['rgb(92, 126, 225)', 'rgb(165, 174, 186)']
    else if (id >= 600)
        return ['rgb(81, 88, 109)', 'rgb(147, 159, 174)']
    else if (id >= 300)
        return ['rgb(15,38,97)', 'rgb(80,120,172)']
    else if (id >= 0)
        return ['rgb(1,7,30)', 'rgb(47,55,81)']
}

export {
    getGradientColors
}