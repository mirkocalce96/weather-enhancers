import React from 'react'
import { createStackNavigator } from "@react-navigation/stack"
import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';

const Stack = createStackNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerBackTitleVisible: false,
                headerTransparent: true,
                headerLeftContainerStyle: { paddingHorizontal: 20 },
                headerBackTitleStyle: { tintColor: 'black' },
                headerTintColor: '#FFF',
                headerRightContainerStyle: { paddingHorizontal: 20 },
                headerTitleStyle: { fontSize: 30, fontWeight: 'bold' }

            }}
        >
            <Stack.Screen
                name="HomeScreen"
                component={HomeScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="DetailScreen"
                component={DetailScreen}
                options={({ route }) => ({
                    headerTitle: route.params?.title ? route.params.title?.toUpperCase() : '',
                    headerTitleAlign: 'center'
                })}
            />
        </Stack.Navigator>
    );
};

export default HomeStack;
