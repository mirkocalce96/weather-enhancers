import { createSlice } from "@reduxjs/toolkit";
import { DEFAULT_CITIES } from "../utils/Constants";

const initialState = {
    cities: DEFAULT_CITIES,
};

const forecastManager = createSlice({
    name: 'forecast',
    initialState: initialState,
    reducers: {
        add: (state, action) => {
            state.cities.forEach(element => {
                if (element.name === action.payload.key)
                    element.current = action.payload.value;
            });
        },
        addOneCall: (state, action) => {
            state.cities.forEach(element => {
                if (element.name === action.payload.key)
                    element.info = action.payload.value;
            });
        }
    },
});

export default forecastManager.reducer