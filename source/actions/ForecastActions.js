function createAddForecastAction(city, forecast) {
    return {
        type: 'forecast/add',
        payload: { key: city, value: forecast },
    };
};

function createAddOneCallAction(city, forecast) {
    return {
        type: 'forecast/addOneCall',
        payload: { key: city, value: forecast },
    };
};

export {
    createAddForecastAction,
    createAddOneCallAction
};
